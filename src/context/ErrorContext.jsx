import React, { createContext, useEffect, useState } from 'react'
import { toast, Bounce } from 'react-toastify'

export const ErrorContext = createContext()

export const ProviderError = (props) => {
  const [objError, setObjError] = useState({
    error: '',
    isError: '',
  })

  useEffect(() => {
    if (objError.isError) {
      toast.error(objError?.error?.message || 'Error desconocido', {
        transition: Bounce,
      })
    }
  }, [objError.isError, objError.error])

  return (
    <ErrorContext.Provider
      value={{
        objError,
        setObjError,
      }}>
      <div className='is-full-height'>{props.children}</div>
    </ErrorContext.Provider>
  )
}
