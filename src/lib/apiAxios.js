const axios = require('axios')
const { selectFetchUrl } = require('./switchFetch')

export const fetchGeneralData = ({
  url,
  pathUrl,
  dataFetch,
  headersObj,
  axiosMethod = 'post',
}) => {
  const completeUrl = `${selectFetchUrl({ url })}/${pathUrl}`

  return axios({
    method: axiosMethod,
    url: completeUrl,
    data: dataFetch,
    headers: {
      ...headersObj,
    },
  })
    .then(({ data }) => data)
    .catch((error) => error)
}

export const fetchGet = ({
  url,
  pathUrl,
  headersObj,
  fetchHeaders = {}, // Example: responseType: 'blob', // important,
}) => {
  const completeUrl = `${selectFetchUrl({ url })}${pathUrl}`

  return axios({
    url: `${completeUrl}`,
    method: 'GET',
    ...fetchHeaders,
    headers: {
      ...headersObj,
    },
  })
    .then((response) => response)
    .catch((error) => error)
}
