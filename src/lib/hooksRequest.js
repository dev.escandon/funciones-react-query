import { useContext, useEffect } from 'react'
import { useMutation, useQuery } from 'react-query'
import { fetchGeneralData, fetchGet } from './apiAxios'
import { ErrorContext } from '../context/ErrorContext'

const VerifyLocalToken = () => {
  const { data } = useQuery('getToken', () => {
    const jwtToken = localStorage.getItem('jwtToken')
    return jwtToken
  })
  return { data }
}

const verifyAuthInfo = ({ configGet, token = '', auth = false }) => {
  let enabledOption = {}
  let headersObj = {}

  if (configGet && Object.keys(configGet).length) {
    enabledOption = configGet.enabled || false
  } else {
    enabledOption = true
  }

  if (auth) {
    if (token) {
      headersObj = {
        Authorization: token,
      }
      configGet = {
        ...configGet,
        enabled: !!(token && enabledOption),
      }
    } else {
      const { data } = VerifyLocalToken()
      headersObj = {
        Authorization: data,
      }
      configGet = {
        ...configGet,
        enabled: !!(data && enabledOption),
      }
    }
  }

  return { headersObj, configGetObj: configGet }
}

export const useFetchRequest = ({
  optionUrl,
  pathUrl,
  token = '',
  auth = false,
  axiosMethod,
  headers = {},
}) => {
  let { headersObj } = verifyAuthInfo({ token, auth })
  headersObj = { ...headersObj, ...headers }

  const fetchData = useMutation(
    async (dataFetch) =>
      await fetchGeneralData({
        optionUrl,
        pathUrl,
        dataFetch,
        headersObj,
        axiosMethod,
      })
  )
  const errorContext = useContext(ErrorContext)

  useEffect(() => {
    if (fetchData.isError && fetchData.error) {
      errorContext.setObjError({
        error: fetchData.error,
        isError: fetchData.isError,
      })
    }
  }, [fetchData.error, fetchData.isError, errorContext])

  return fetchData
}

export const useGetRequest = ({
  keyRequest,
  optionUrl,
  pathUrl,
  configGet = {},
  auth = false,
  token = '',
  fetchHeaders,
  arrayKeys = [],
}) => {
  const errorContext = useContext(ErrorContext)
  const { headersObj, configGetObj } = verifyAuthInfo({
    configGet,
    token,
    auth,
  })

  const getData = useQuery(
    [keyRequest, ...arrayKeys],
    async () =>
      await fetchGet({ optionUrl, pathUrl, headersObj, fetchHeaders }),
    {
      ...configGetObj,
    }
  )

  useEffect(() => {
    if (getData.isError && getData.error) {
      errorContext.setObjError({
        error: getData.error,
        isError: getData.isError,
      })
    }
  }, [getData.error, getData.isError, errorContext])

  return getData
}

