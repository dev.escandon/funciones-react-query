const defaultApi = process.env.REACT_APP_POKEMON
const catsApi = process.env.REACT_APP_CATS

export const selectFetchUrl = ({ url }) => {
  switch (url) {
    case 'catsApi':
      return catsApi

    default:
      return defaultApi
  }
}

