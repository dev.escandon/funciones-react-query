import React, { useEffect, useState } from 'react'
import { useGetRequest } from '../lib/hooksRequest'
import Card from './Card'

const PokemonContainer = () => {
  const pokemonData = useGetRequest({
    keyRequest: 'allPokemon',
    pathUrl: '/pokemon?limit=100',
  })
  const [arrayPokemon, setArrayPokemon] = useState([])

  useEffect(() => {
    if (pokemonData.isSuccess && pokemonData.data) {
      setArrayPokemon(pokemonData.data.data.results)
    }
  }, [pokemonData.data, pokemonData.isSuccess])


  return (
    <div>
      <div className='mb-4'>
      </div>
      <div className='columns is-3 is-multiline is-mobile'>
        {arrayPokemon.map((pokemonOption) => {
          return <Card key={pokemonOption.name} {...pokemonOption} />
        })}
      </div>
    </div>
  )
}

export default PokemonContainer
