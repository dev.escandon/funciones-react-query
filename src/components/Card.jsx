import React from 'react'

const Card = ({ name }) => {
  return (
    <div className='column is-12-mobile is-4-tablet is-3-desktop'>
      <div className='box has-background-success'>
        <a
          href={`https://www.pokemon.com/us/pokedex/${name}`}
          target='_blank'
          rel='noreferrer'>
          <div className='image is-1by1'>
            <img
              alt='pokemon'
              src={`https://img.pokemondb.net/artwork/large/${name}.jpg`}
            />
          </div>
          <h2 className='has-text-white has-text-centered is-size-4 mt-3 has-text-weight-bold'>{name[0].toUpperCase() + name.slice(1)}</h2>
        </a>
      </div>
    </div>
  )
}

export default Card
