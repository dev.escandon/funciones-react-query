import './sass/main.scss'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ToastContainer, Zoom } from 'react-toastify'
import PokemonContainer from './components/PokemonContainer'
import { ProviderError } from './context/ErrorContext'

function App() {
  // Crea un cliente
  const queryClient = new QueryClient()

  return (
    <div className='has-background-danger'>
      <ProviderError>
        <ToastContainer hideProgressBar transition={Zoom} />
        <QueryClientProvider client={queryClient}>
          <div className='p-5'>
            <PokemonContainer />
          </div>
        </QueryClientProvider>
      </ProviderError>
    </div>
  )
}

export default App
